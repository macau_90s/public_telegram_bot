from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import Job
from read_pass_db import get_pass

# ------ Reddit Bot

def fetch_subreddit_top(N,subreddit):
    import praw

    username = get_pass('reddit_username')
    userAgent = "MyAppName/0.1 by " + username
    clientId = get_pass('reddit_client_id')
    clientSecret = get_pass('reddit_client_secret')
    password = get_pass('reddit_password')

    global accessToken
    accessToken = None
    
    global prawReddit
    r = praw.Reddit(user_agent=userAgent, client_id=clientId, client_secret=clientSecret)
    subreddit = r.get_subreddit(subreddit)
    
    titles = []
    urls = []
    scores = []
    for n,submission in enumerate(subreddit.get_hot(limit = N)):
        titles.append(submission.title)
        urls.append(submission.url)
        scores.append(submission.score)
    
    return titles,urls,scores

# ------ Debates
def debates(bot, update, args):
    import Debates
    reload(Debates)
    d = Debates.Debates()
    txt = d.arg_decoder(args)
    print txt
    if isinstance(txt,basestring):
        bot.sendMessage(chat_id=update.message.chat_id, text=txt)


    else:
        txt1 = ''
        for i in range(len(txt)):
            txt1 += txt[i]+'\n'
            if (i+1)%20 == 0:
                bot.sendMessage(chat_id=update.message.chat_id, text=txt1)
                txt1 = ''
        
        if txt1 !='':        
            bot.sendMessage(chat_id=update.message.chat_id, text=txt1)         

# ------ Macau facts

def get_fact(n,infile):
    import re

    exp = r'^(%d\)\s)(.*)' % n

    with open(infile) as f:
        res = re.search(exp,f.read(),re.MULTILINE)
        if res != []:
            return res.group(2)

    return 'No results'

def macau_facts(bot, update):
    from random import randint

    n = randint(1,45)
    infile = 'macau_facts.txt'
    fact = get_fact(n,infile)
    bot.sendMessage(chat_id=update.message.chat_id, text=fact)

#------ Bender quotes
def bender_quotes(bot, update):
    from random import randint

    n = randint(1,23)
    print n
    infile = 'bender_quotes.txt'
    print infile
    quote = get_fact(n,infile)
    print quote
    bot.sendMessage(chat_id=update.message.chat_id, text=quote)

def callback_alarm(bot, job):
    from random import randint

    n = randint(1,23)
    print n
    infile = 'bender_quotes.txt'
    print infile
    quote = get_fact(n,infile)
    print quote
    bot.sendMessage(chat_id=job.context, text=quote)

def callback_timer(bot, update, job_queue):
    time_int = 3600.*4.
    bot.sendMessage(chat_id=update.message.chat_id, text='Starting Bender...')
    job_alarm = Job(callback_alarm,time_int,context=update.message.chat_id)
    job_queue.put(job_alarm, next_t = 0.)

def stop_callback_timer(bot, update, job_queue):
    bot.sendMessage(chat_id=update.message.chat_id, text='Stopping Bender...')
    job_queue.stop()


# ------ Wolfram

def get_alpha_client():
    import wolframalpha

    appid=get_pass('wolfram_appid')
    print appid
    client = wolframalpha.Client(appid)

    return client

def make_underline(N):
    underline = ''
    symbol = '='
    for i in xrange(N):
        underline += symbol
        
    return underline

def alpha(bot, update, args):

    #query = 'temperature in Macau on September 17, 1992'
    query = ' '.join(args)
    print query
    
    client = get_alpha_client()
    res = client.query(query)

    result = ''
    for pod in res.pods:
        print(pod.text)
        if pod.text != None:
            title = r'%s:' % pod.title
            print(title)
            print(pod.text)
            result += title+'\n'+make_underline(len(title))+'\n'+pod.text+'\n\n'
        try:
            for sub in pod.subpods:
                if sub.text != None:
                    title = r'%s:' % sub.title
                    print title
                    print(sub.text)
                    result += title+'\n'+make_underline(len(title))+'\n'+sub.text+'\n\n'
        except:
            pass

    bot.sendMessage(chat_id=update.message.chat_id, text=result)

def alpha_img(bot, update, args):

    #query = 'temperature in Macau on September 17, 1992'
    query = ' '.join(args)
    print query

    client = get_alpha_client()
    res = client.query(query)

    for pod in res.pods:
        print(pod.text)
        if pod.text != None:
            title = r'%s:' % pod.title
            print(title)
            print(pod.text)
            bot.sendMessage(chat_id=update.message.chat_id, text=title)
            bot.sendPhoto(chat_id=update.message.chat_id, photo=pod.img)
        try:
            for sub in pod.subpods:
                if sub.text != None:
                    title = r'%s:' % sub.title
                    print title
                    print(sub.text)
                    bot.sendMessage(chat_id=update.message.chat_id, text=title)
                    bot.sendPhoto(chat_id=update.message.chat_id, photo=sub.img)
        except:
            pass

# ------ Take screenshot of a page

def screenshot(url,c = 1024,r = 2160):
    from selenium import webdriver

    driver = webdriver.PhantomJS()
    #driver = webdriver.Firefox()
    #driver.set_window_size(c, r) # optional
    driver.maximize_window()
    driver.set_page_load_timeout(15) # set time out
    try:
        driver.get(url)
        driver.save_screenshot('screenshot.png') # save a screenshot to disk
        print('Saved screenshot')
        driver.quit()
        timeout = 0
    except:
        print('Timed out')
        driver.quit()
        timeout = 1

    return timeout
    
# ------ Send photo

def img(bot, update, args):

    url = ' '.join(args)
    print('url: %s' % url)

    print('    ... getting screenshot ...')
    timeout = screenshot(url)
    
    if timeout:
        err_msg = 'Sorry couldnt get the screenshot.'
        bot.sendMessage(chat_id=update.message.chat_id, text=err_msg)
    else:
        print('    ... opening screenshot ...')
        f = open('screenshot.png', 'rb')

        print('    ... sending screenshot ...')
        #bot.sendPhoto(chat_id=update.message.chat_id, photo=f)
        bot.sendDocument(chat_id=update.message.chat_id, document=f)
        print('    ... sent screenshot ...')

# ------ Google image search

def murica(bot, update, args):
    import urllib2
    import simplejson
    import cStringIO
    
    fetcher = urllib2.build_opener()
    searchTerm = 'parrot'
    startIndex = 0
    searchUrl = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + searchTerm + "&start=" + startIndex
    f = fetcher.open(searchUrl)
    deserialized_output = simplejson.load(f)
    imageUrl = deserialized_output['responseData']['results'][0]['unescapedUrl']
    file = cStringIO.StringIO(urllib.urlopen(imageUrl).read())
    img = Image.open(file)

    bot.sendDocument(chat_id=update.message.chat_id, document=img)

# ------- News source verifier

def get_newspaper_title(url):
    from newspaper import Article

    article = Article(url)
    article.download()
    article.parse()
    
    return article.title 

def get_page_tree(url):
    from lxml import html
    import requests

    page = requests.get(url=url, verify=False)
    return html.fromstring(page.text)

def find_other_news_sources(title):
    import urllib2

    # Google forwards the url using <google_domain>/url?q=    <actual_link>. This might change over time
    forwarding_identifier = '/url?q='
    print title
    params = ['&tbm=nws','&scoring=n','&num=100','&as_qdr=d1']
    google_news_search_url = 'http://www.google.com/search?q=' + urllib2.quote(title) + params[0] + params[1] + params[2] + params[3]
    print google_news_search_url
    google_news_search_tree = get_page_tree(url=google_news_search_url)
    other_news_sources_links = [a_link.replace(forwarding_identifier, '').split('&')[0] for a_link in
                            google_news_search_tree.xpath('//a//@href') if forwarding_identifier in a_link]

    return other_news_sources_links, google_news_search_url

def news_verifier(bot, update, args):

    title = ' '.join(args)

    if title[0:4] == r'http':
        title = get_newspaper_title(title)
    onsl, gnsu = find_other_news_sources(title)
    n_results = len(onsl)

    txt1 = 'Today there are %d other results in google news for the article with title: %s\n' % (n_results,title) 
    bot.sendMessage(chat_id=update.message.chat_id, text=txt1)

    p_results = 5
    if n_results > 0:
        
        if n_results < 5:
            p_results = n_results

        txt2 = 'These are the %d most recent news sources links:\n' % p_results
        bot.sendMessage(chat_id=update.message.chat_id, text=txt2)

        txt3 = ''

        for i in range(p_results):
            #        temp = onsl[i].split("//")[-1].split("/")[0]
            #        txt3[i], ext = temp.split('.')[-2:]
            txt3 = txt3 + onsl[i] + '\n\n'
        print txt3
        bot.sendMessage(chat_id=update.message.chat_id, text=txt3)

        txt4 = 'This is the google news URL I used :\n%s' % gnsu
    
    bot.sendMessage(chat_id=update.message.chat_id, text=txt4)

# --------- Conspiracy
        
def random_conspiracy(bot, update, args):
    import random

    try: 
        N = args[0]
        print N
        if N.isdigit():
            N = int(N)
            if N > 100:
                N = 100
            if N < 1:
                N = 10
        else:
            N = 10
    except:
        N = 10
    print N
    titles,urls,scores = fetch_subreddit_top(N,'conspiracy')

    txt0 = 'Random conspiracy for today out of %d conspiracies:' % N
    print txt0
    bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
    
    found = False
    num = 0
    while found == False:
        num += 1
        n = random.randrange(0,N)
        print n,titles[n]
        if 'reddit' in urls[n]:
            pass
        else:
            txt1 = '%s, (conspiracy score = %.2f%%)' % (titles[n],float(scores[n])/max(scores)*100)
            txt2 = urls[n]
            print txt1
            print txt2

            bot.sendMessage(chat_id=update.message.chat_id, text=txt1)
            bot.sendMessage(chat_id=update.message.chat_id, text=txt2)
            found = True
        if num > 3*n:
            txt0 = 'Cannot find any conspiracies today out of %d conspiracies (try increasing N)' % N
            print txt0
            bot.sendMessage(chat_id=update.message.chat_id, text=txt0)

            return

def random_future(bot, update, args):
    import random

    try: 
        N = args[0]
        print N
        if N.isdigit():
            N = int(N)
            if N > 100:
                N = 100
            if N < 1:
                N = 10
        else:
            N = 10
    except:
        N = 10
    print N
    titles,urls,scores = fetch_subreddit_top(N,'futurology')

    txt0 = 'Random future story for today out of %d:' % N
    print txt0
    bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
    
    found = False
    num = 0
    while found == False:
        num += 1
        n = random.randrange(0,N)
        print n,titles[n]
        if 'reddit' in urls[n]:
            pass
        else:
            txt1 = '%s, (future score = %.2f%%)' % (titles[n],float(scores[n])/max(scores)*100)
            txt2 = urls[n]
            print txt1
            print txt2

            bot.sendMessage(chat_id=update.message.chat_id, text=txt1)
            bot.sendMessage(chat_id=update.message.chat_id, text=txt2)
            found = True
        if num > 3*n:
            txt0 = 'Cannot find any future stories today out of %d (try increasing N)' % N
            print txt0
            bot.sendMessage(chat_id=update.message.chat_id, text=txt0)

            return

def random_gaming(bot, update, args):
    import random

    try: 
        N = args[0]
        print N
        if N.isdigit():
            N = int(N)
            if N > 100:
                N = 100
            if N < 1:
                N = 10
        else:
            N = 10
    except:
        N = 10
    print N
    titles,urls,scores = fetch_subreddit_top(N,'gaming')

    txt0 = 'Random gaming story for today out of %d:' % N
    print txt0
    bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
    
    found = False
    num = 0
    while found == False:
        num += 1
        n = random.randrange(0,N)
        print n,titles[n]
        if 'reddit' in urls[n]:
            pass
        else:
            txt1 = '%s, (gaming score = %.2f%%)' % (titles[n],float(scores[n])/max(scores)*100)
            txt2 = urls[n]
            print txt1
            print txt2

            bot.sendMessage(chat_id=update.message.chat_id, text=txt1)
            bot.sendMessage(chat_id=update.message.chat_id, text=txt2)
            found = True
        if num > 3*n:
            txt0 = 'Cannot find any gaming stories today out of %d (try increasing N)' % N
            print txt0
            bot.sendMessage(chat_id=update.message.chat_id, text=txt0)

            return

def random_computers(bot, update, args):
    import random

    try: 
        N = args[0]
        print N
        if N.isdigit():
            N = int(N)
            if N > 100:
                N = 100
            if N < 1:
                N = 10
        else:
            N = 10
    except:
        N = 10
    print N
    titles,urls,scores = fetch_subreddit_top(N,'technology')

    txt0 = 'Random computers story for today out of %d:' % N
    print txt0
    bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
    
    found = False
    num = 0
    while found == False:
        num += 1
        n = random.randrange(0,N)
        print n,titles[n]
        if 'reddit' in urls[n]:
            pass
        else:
            txt1 = '%s, (computers score = %.2f%%)' % (titles[n],float(scores[n])/max(scores)*100)
            txt2 = urls[n]
            print txt1
            print txt2

            bot.sendMessage(chat_id=update.message.chat_id, text=txt1)
            bot.sendMessage(chat_id=update.message.chat_id, text=txt2)
            found = True
        if num > 3*n:
            txt0 = 'Cannot find any computers stories today out of %d (try increasing N)' % N
            print txt0
            bot.sendMessage(chat_id=update.message.chat_id, text=txt0)

            return

# ---------- Random subreddit

def random_subreddit(bot, update, args):
    import random

    #argsin = ' '.join(args)
    print args
    nargs = len(args)
    if nargs == 0:
        tx0 = r'You forgot to choose a topic. Try again.'
        print txt0
        bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
        return
    elif nargs == 1:
        N = 10
        subreddit = args[0]
    else:
        subreddit = args[0]
        N = args[1]
    
        if N.isdigit():
            N = int(N)
            if N > 100:
                N = 100
            if N < 1:
                N = 10
        else:
            N = 10

    try:
        titles,urls,scores = fetch_subreddit_top(N,subreddit)
    except:
        txt0 = r'Sorry, I can not find any topics for %s.' % subreddit
        print txt0
        bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
        return

    txt0 = 'Random %s thing for today (out of %d):' % (subreddit,N)

    print txt0
    bot.sendMessage(chat_id=update.message.chat_id, text=txt0)
    
    found = False
    while found == False:
        n = random.randrange(0,N)
        if 'reddit' in urls[n]:
            pass
        else:
            txt1 = '%s, (%s score = %.2f%%)' % (titles[n],subreddit,float(scores[n])/max(scores)*100)
            txt2 = urls[n]
            print txt1
            print txt2

            bot.sendMessage(chat_id=update.message.chat_id, text=txt1)
            bot.sendMessage(chat_id=update.message.chat_id, text=txt2)
            found = True

def callback_minute(bot, job):
    bot.sendMessage(chat_id=70792619, text='One msg every min')

chat_id = get_pass('telegram_chat_id')
token = get_pass('telegram_token')
updater = Updater(token=token)

#j = updater.job_queue
#job_minute = Job(callback_minute, 60.0)
#j.put(job_minute, next_t=0.0)

dispatcher = updater.dispatcher

timer_bender_quotes_handler = CommandHandler('start_bender', callback_timer, pass_job_queue=True)
dispatcher.add_handler(timer_bender_quotes_handler)

stop_timer_bender_quotes_handler = CommandHandler('stop_bender', stop_callback_timer, pass_job_queue=True)
dispatcher.add_handler(stop_timer_bender_quotes_handler)

alpha_handler = CommandHandler('alpha', alpha, pass_args=True)
dispatcher.add_handler(alpha_handler)

alpha_img_handler = CommandHandler('alpha_img', alpha_img, pass_args=True)
dispatcher.add_handler(alpha_img_handler)

img_handler = CommandHandler('img', img, pass_args=True)
dispatcher.add_handler(img_handler)

macau_facts_handler = CommandHandler('facts', macau_facts, pass_args=False)
dispatcher.add_handler(macau_facts_handler)

bender_quotes_handler = CommandHandler('quotes', bender_quotes, pass_args=False)
dispatcher.add_handler(bender_quotes_handler)

murica_handler = CommandHandler('murica', murica, pass_args=False)
dispatcher.add_handler(murica_handler)

news_verifier_handler = CommandHandler('news_verifier', news_verifier, pass_args=True)
dispatcher.add_handler(news_verifier_handler)

random_conspiracy_handler = CommandHandler('conspiracy', random_conspiracy, pass_args=True)
dispatcher.add_handler(random_conspiracy_handler)
 
random_computers_handler = CommandHandler('computers', random_computers, pass_args=True)
dispatcher.add_handler(random_computers_handler)

random_future_handler = CommandHandler('future', random_future, pass_args=True)
dispatcher.add_handler(random_future_handler)

random_gaming_handler = CommandHandler('gaming', random_gaming, pass_args=True)
dispatcher.add_handler(random_gaming_handler)

random_subreddit_handler = CommandHandler('topic', random_subreddit, pass_args=True)
dispatcher.add_handler(random_subreddit_handler)

debates_handler = CommandHandler('debates', debates, pass_args=True)
dispatcher.add_handler(debates_handler)

#start the bot
updater.start_polling()
updater.idle()
